package com.mx.bankaccountapp;

import java.util.LinkedList;
import java.util.List;

public class BankAccountApp {
    public static void main(String[] args) {
        List<Account> accounts = new LinkedList<Account>();


        // Read a CSV File then create new accounts based on that rate
        String file = "/Users/ix1803/Desktop/original.csv";
        List<String[]> newAccountHolders = com.mx.bankaccountapp.utilities.CSV.read(file);
        for (String[] newAccountHolder : newAccountHolders){
            String name = newAccountHolder[0];
            String sSN = newAccountHolder[1];
            String accountType = newAccountHolder[2];
            double initDeposit = Double.parseDouble(newAccountHolder[3]);

            // System.out.println(name + " " + sSN + " " + accountType + " " + initDeposit );

            if (accountType.equals("Savings")){
                accounts.add(new Savings(name,sSN,initDeposit));
            }
            else if (accountType.equals("Checking")){
                accounts.add(new Checking(name,sSN,initDeposit));
            } else {
                System.out.println("ERROR READING ACCOUNT TYPE");
            }

        }

        //System.out.println(accounts.get(5).toString());

        for (Account acc : accounts){
            System.out.println("**********************");
            System.out.println(acc.toString());
        }

    }
}
