package com.mx.bankaccountapp;

public class Savings extends Account {
    // List properties specific to the Savings account
    private int safetyDepositBoxID;
    private int getSafetyDepositBoxKey;

    // Constructor to initialize settings for the Saving accounts
    public Savings(String name, String sSN, double initDeposit){
        super(name, sSN, initDeposit);
        accountNumber = "1" + accountNumber;

        setSafetyDepositBox();
    }

    @Override
    public void setRate(){
        rate = getBaseRate() - .25;
    }

    // List of any methods specific to Savings account

    private void setSafetyDepositBox(){
        safetyDepositBoxID = (int) (Math.random() * Math.pow(10,3));
        getSafetyDepositBoxKey = (int) (Math.random() * Math.pow(10,4));
    }

    @Override
    public String toString() {
        return "\nNEW ACCOUNT" +
                "\nTYPE: Savings\n" +
                super.toString() +
                "\n     Safety Deposit Box ID is: " + safetyDepositBoxID +
                "\n     Safety Deposit Box Key is: " + getSafetyDepositBoxKey;
    }
}
