package com.mx.bankaccountapp;

public abstract class Account implements IBaseRate {
    // List common properties for savings and checking accounts
    private String name;
    private String sSN;
    private double balance;

    private static int index = 10000;
    protected String accountNumber;
    protected double rate;

    // Constructor to set base properties and initialize the account
    public Account(String name, String sSN, double initDeposit){
        this.name = name;
        this.sSN = sSN;
        balance = initDeposit;

        // Set account number
        index++;
        this.accountNumber = setAccountNumber();


        setRate();
    }

    public abstract void setRate();

    private String setAccountNumber(){
        String lastTwoSSN = sSN.substring(sSN.length()-2,sSN.length());
        int uniqueID = index;
        int randNum = (int) (Math.random() * Math.pow(10,3));
        return lastTwoSSN + uniqueID + randNum;
    }

    public void compound(){
        double accruedInterest = balance * (rate/100); //rate belong to the object (checking/saving)
        balance = balance + accruedInterest;
        System.out.println("Accrued Interest: $" + accruedInterest);
        printBalance();
    }

    // List common transactions methods

    public void deposit(double amount){
        balance = balance + amount;
        System.out.println("Depositing $" + amount);
        printBalance();

    }

    public void withdraw(double amount){
        balance = balance - amount;
        System.out.println("Withdrawing $" + amount);
        printBalance();
    }

    public void transfer(String toWhere, double amount){
        balance = balance - amount;
        System.out.println("Transfering $" + amount + " to " + toWhere);
        printBalance();
    }

    public void printBalance(){
        System.out.println("Your balance is now $" + balance);
    }


    @Override
    public String toString() {
        return "  Name: " + name +
                "\n  SSN: " + sSN +
                "\n  Balance: " + balance +
                "\n  AccountNumber: " + accountNumber +
                "\n  RATE: " + rate + "%";
    }
}
