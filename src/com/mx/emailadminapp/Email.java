package com.mx.emailadminapp;

import java.util.Scanner;

public class Email {
    private String firstName;
    private String lastName;
    private String password;
    private String department;
    private String email;
    private int mailboxCapacity = 500;
    private int defaultPasswordLength = 10;
    private String alternateEmail;
    private String companySuffix = "company.com";

    // Contructor to receive firstname and last name
    public Email(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;

        // Call a method asking fot the department - return the department
        this.department = setDepartment();

        // Call a method that return a random password
        this.password = randomPassworod(defaultPasswordLength);
        System.out.println("Your password is:" + this.password);

        // Combine elements to generate email
        email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + this.department + "." + this.companySuffix;
    }

    // Ask for the department
    private String setDepartment(){
        System.out.println("DEPARTMENT CODES:\n1. for Sales\n2. for Development\n3. for Accounting\n0. for none\nEnter the department");
        Scanner sn = new Scanner(System.in);
        int depChoice = sn.nextInt();
        if (depChoice == 1) { return "sales"; }
        else if (depChoice == 2) { return "dev"; }
        else if (depChoice == 3) { return "acct"; }
        else { return ""; }
    }

    // Generate a random password
    private String randomPassworod(int length){
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@#$%";
        char[] password = new char[length];
        for (int i = 0; i < length; i++){
            int rand = (int) (Math.random() * passwordSet.length());
            password[i] = passwordSet.charAt(rand);
        }
        return new String(password);
    }

    // Set the mailbox capacity
    public void setMailboxCapacity(int capacity){
        this.mailboxCapacity = capacity;
    }

    // Set alternate email
    public void setAlternateEmail(String alternateEmail){
        this.alternateEmail = alternateEmail;
    }

    // Change the password
    public void changePassword(String password){
        this.password = password;
    }

    public int getMailboxCapacity(){
        return this.mailboxCapacity;
    }

    public String getAlternateEmail(){
        return this.alternateEmail;
    }

    public String getPassword(){
        return this.password;
    }

    public String toString() {
        return "Display name: " + this.firstName + " " + this.lastName + "\n" +
                "Company Email: " + this.email + "\n" +
                "Mailbox Capacity: " + this.mailboxCapacity + "mb";
    }
}
